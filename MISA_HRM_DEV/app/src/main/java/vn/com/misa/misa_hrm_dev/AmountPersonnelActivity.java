package vn.com.misa.misa_hrm_dev;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class AmountPersonnelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount_personnel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Không hiện tiêu đề
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Hiện nút back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BarChart barChart = (BarChart) findViewById(R.id.barchart);
        ArrayList<BarEntry> entries=new ArrayList<>();
        entries.add(new BarEntry(106,0));
        entries.add(new BarEntry(105,1));
        entries.add(new BarEntry(122,2));
        entries.add(new BarEntry(121,3));
        entries.add(new BarEntry(124,4));
        entries.add(new BarEntry(126,5));
        entries.add(new BarEntry(128,6));
        entries.add(new BarEntry(132,7));
        entries.add(new BarEntry(148,8));
        entries.add(new BarEntry(157,9));
        entries.add(new BarEntry(168,10));
        entries.add(new BarEntry(175,11));
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("T1");
        labels.add("T2");
        labels.add("T3");
        labels.add("T4");
        labels.add("T5");
        labels.add("T6");
        labels.add("T7");
        labels.add("T8");
        labels.add("T9");
        labels.add("T10");
        labels.add("T11");
        labels.add("T12 ");
        BarDataSet bardataset = new BarDataSet(entries, "nhân sự");
        BarData data = new BarData(labels, bardataset);
        barChart.setData(data); // set the data and list of labels into chart
        barChart.setDescription("Set Bar Chart Description");  // set the description
        barChart.animateY(2000  );
    }

    //inflate menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
