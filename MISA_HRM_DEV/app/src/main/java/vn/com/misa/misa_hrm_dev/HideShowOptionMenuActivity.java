package vn.com.misa.misa_hrm_dev;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class HideShowOptionMenuActivity extends AppCompatActivity {

    private Menu menu;

    private boolean isShow=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hide_show_option_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Không hiện tiêu đề
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Hiện nút back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnHideShow=findViewById(R.id.btn_hide_show);
        btnHideShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShow=!isShow;
                showOverFlowMenu(isShow);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        this.menu=menu;
        return super.onCreateOptionsMenu(menu);
    }

    public void showOverFlowMenu(boolean showMenu){
        if (menu==null) return;;
        menu.setGroupVisible(R.id.action,showMenu);
    }
}
