package vn.com.misa.misa_hrm_dev;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewCheckBoxActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_check_box);

        recyclerView=findViewById(R.id.recycler_view);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);

        List<Student> students=new ArrayList<>();
        students.add(new Student("Le Van Vu 1","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 2","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 3","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 4","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 5","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 6","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 7","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 8","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 9","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 10","Dai hoc Bach khoa Ha Noi"));
        students.add(new Student("Le Van Vu 11","Dai hoc Bach khoa Ha Noi"));

        final MyAdapter adapter=new MyAdapter(students,RecyclerViewCheckBoxActivity.this,R.layout.recycler_item_layout);
        recyclerView.setAdapter(adapter);

        Button btnShow=findViewById(R.id.btn_show);
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RecyclerViewCheckBoxActivity.this,adapter.getSelected(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder>{

    private List<Student> students;

    private Context context;

    private int resource;

    public MyAdapter(List<Student> students, Context context, int resource) {
        this.students = students;
        this.context = context;
        this.resource = resource;
    }

    public List<Student> getStudents() {
        return students;
    }

    public Context getContext() {
        return context;
    }

    public int getResource() {
        return resource;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(resource,parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        final Student student=students.get(position);
        holder.txtName.setText(student.getName());
        holder.txtSchool.setText(student.getSchool());
        holder.checkBox.setChecked(student.isChecked());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                student.setChecked(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{

        public TextView txtName,txtSchool;

        public CheckBox checkBox;

        public MyHolder(View itemView) {
            super(itemView);

            txtName=itemView.findViewById(R.id.txt_name);
            txtSchool=itemView.findViewById(R.id.txt_school);
            checkBox=itemView.findViewById(R.id.check_box);
        }
    }

    public String getSelected(){
        String str="";
        for (Student s: students){
            if (s.isChecked()==true){
                str+="\n"+s.getName();
            }
        }
        return str;
    }
}

class Student{
    private String name;

    private String school;

    private boolean checked=false;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Student(String name, String school) {

        this.name = name;
        this.school = school;
    }
}
