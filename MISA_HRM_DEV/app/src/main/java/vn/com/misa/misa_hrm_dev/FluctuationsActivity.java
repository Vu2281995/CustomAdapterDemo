package vn.com.misa.misa_hrm_dev;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class FluctuationsActivity extends AppCompatActivity {

    private LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fluctuations);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Không hiện tiêu đề
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Hiện nút back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lineChart=findViewById(R.id.line_chart);

        setData();

        // get the legend (only possible after setting data)
        Legend l = lineChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
    }

    //inflate menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("T1");
        xVals.add("T2");
        xVals.add("T3");
        xVals.add("T4");
        xVals.add("T5");
        xVals.add("T6");
        xVals.add("T7");
        xVals.add("T8");
        xVals.add("T9");
        xVals.add("T10");
        xVals.add("T11");
        xVals.add("T12");

        return xVals;
    }

    private ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(8, 0));
        yVals.add(new Entry(2, 1));
        yVals.add(new Entry(2, 2));
        yVals.add(new Entry(0, 3));
        yVals.add(new Entry(0, 4));
        yVals.add(new Entry(4, 5));
        yVals.add(new Entry(9, 6));
        yVals.add(new Entry(4, 7));
        yVals.add(new Entry(3, 8));
        yVals.add(new Entry(5, 9));
        yVals.add(new Entry(2, 10));
        yVals.add(new Entry(1, 11));

        return yVals;
    }

    private ArrayList<Entry> setYAxisValues2(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(4, 0));
        yVals.add(new Entry(5, 1));
        yVals.add(new Entry(7, 2));
        yVals.add(new Entry(9, 3));
        yVals.add(new Entry(3, 4));
        yVals.add(new Entry(2, 5));
        yVals.add(new Entry(1, 6));
        yVals.add(new Entry(0, 7));
        yVals.add(new Entry(8, 8));
        yVals.add(new Entry(7, 9));
        yVals.add(new Entry(11, 10));
        yVals.add(new Entry(2, 11));

        return yVals;
    }

    private void setData() {
        ArrayList<String> xVals = setXAxisValues();

        ArrayList<Entry> yVals = setYAxisValues();

        ArrayList<Entry> yVals2=setYAxisValues2();

        LineDataSet set1,set2;

        // create a dataset and give it a type
        set1 = new LineDataSet(yVals, "Nhân sự nghỉ việc");
        set2=new LineDataSet(yVals2,"Nhân sự tuyển mới");
        set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.RED);
        set1.setCircleColor(Color.RED);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(false);

        set2.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(Color.BLUE);
        set2.setCircleColor(Color.BLUE);
        set2.setLineWidth(1f);
        set2.setCircleRadius(3f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(9f);
        set2.setDrawFilled(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);// add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        lineChart.setData(data);
        lineChart.setDescription("biến động nhân sự");

    }

}
