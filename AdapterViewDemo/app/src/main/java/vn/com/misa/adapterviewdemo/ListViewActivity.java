package vn.com.misa.adapterviewdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by levan on 12/3/2017.
 */

public class ListViewActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> data;
    private CustomAdapter adapter;

    public void onCreate(Bundle savedInstanceStated){
        super.onCreate(savedInstanceStated);
        setContentView(R.layout.activity_list_view);

        setTitle("ListView");

        listView = findViewById(R.id.list_view);

        data=new ArrayList<>();

        adapter=new CustomAdapter(data,ListViewActivity.this,R.layout.item_layout);
        listView.setAdapter(adapter);
        loadData();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListViewActivity.this,data.get(position),Toast.LENGTH_SHORT).show();
            }
        });
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Option");
        menu.add(0, v.getId(), 0, "Edit");//groupId, itemId, order, title
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
       if (item.getTitle()=="Edit"){
           final Dialog dialog=new Dialog(ListViewActivity.this);
           dialog.setContentView(R.layout.update_dialog_layout);
           dialog.setTitle("Edit");
           final EditText edt=dialog.findViewById(R.id.edt_object_content);
           edt.setText(data.get(info.position));
           Button btnCancel=dialog.findViewById(R.id.btn_cancel);
           Button btnSave=dialog.findViewById(R.id.btn_save);

           btnCancel.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   dialog.dismiss();
               }
           });

           btnSave.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   String newValue=edt.getText().toString();
                   data.set(info.position,newValue);
                   adapter.notifyDataSetChanged();
                   dialog.dismiss();
               }
           });

           dialog.show();

           return true;
       }
       if (item.getTitle()=="Delete"){
           data.remove(info.position);
           adapter.notifyDataSetChanged();
           return true;
       }
       return false;
    }

    private void loadData(){
        new AsyncTask<Void, String, Void>() {
            ProgressDialog progressDialog;
            @Override
            protected void onPreExecute() {
                progressDialog=new ProgressDialog(ListViewActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setTitle("Load Data");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                for (int i = 0; i < 100; i++) {
                    publishProgress("Object "+i);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Log.e("MainActivity",e.getMessage());
                    }
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                data.add(values[0]);
                adapter.notifyDataSetChanged();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.dismiss();
            }
        }.execute();
    }


    class CustomAdapter extends BaseAdapter {

        private List<String> data;
        private Context context;
        private int resource;

        public CustomAdapter(List<String> data, Context context, int resource) {
            this.data = data;
            this.context = context;
            this.resource = resource;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CustomAdapter.CustomHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(resource, parent, false);
                holder = new CustomAdapter.CustomHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (CustomAdapter.CustomHolder) convertView.getTag();
            }
            holder.txt.setText(data.get(position));
            return convertView;
        }

        class CustomHolder {
            public TextView txt;

            public CustomHolder(View itemView) {
                txt = itemView.findViewById(R.id.txt);
            }
        }
    }

}
