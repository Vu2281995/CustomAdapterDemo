package vn.com.misa.adapterviewdemo;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by levan on 12/3/2017.
 */

public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<String> data;
    private CustomAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onCreate(Bundle savedInstanceStated){
        super.onCreate(savedInstanceStated);
        setContentView(R.layout.activity_recycler_view);

        setTitle("RecyclerView");

        recyclerView = findViewById(R.id.recycler_view);

        data=new ArrayList<>();

        adapter=new CustomAdapter(data,R.layout.item_layout,RecyclerViewActivity.this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable dividerDrawable=getDrawable(R.drawable.line_divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        loadData();
    }

    private void loadData(){
        new AsyncTask<Void, String, Void>() {
            ProgressDialog progressDialog;
            @Override
            protected void onPreExecute() {
                progressDialog=new ProgressDialog(RecyclerViewActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setTitle("Load Data");
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                for (int i = 0; i < 100; i++) {
                    publishProgress("Object "+i);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Log.e("MainActivity",e.getMessage());
                    }
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                data.add(values[0]);
                adapter.notifyItemInserted(data.size());
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.dismiss();
            }
        }.execute();
    }

    class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomHolder> {

        private List<String> data;
        private int resource;
        private Context context;

        public CustomAdapter(List<String> data, int resource, Context context) {
            this.data = data;
            this.context = context;
            this.resource = resource;
        }

        @Override
        public CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context).inflate(resource, parent, false);
            CustomHolder holder = new CustomHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomHolder holder, final int position) {
            holder.txt.setText(data.get(position));
            holder.linearItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(RecyclerViewActivity.this,data.get(position),Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class CustomHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

            public TextView txt;

            public LinearLayout linearItem;

            public CustomHolder(View itemView) {
                super(itemView);

                txt = itemView.findViewById(R.id.txt);
                linearItem=itemView.findViewById(R.id.linear_item);

                itemView.setOnCreateContextMenuListener(this);
            }

            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuItem Edit = menu.add(Menu.NONE, 1, 1, "Edit");
                MenuItem Delete = menu.add(Menu.NONE, 2, 2, "Delete");
                Edit.setOnMenuItemClickListener(onEditMenu);
                Delete.setOnMenuItemClickListener(onEditMenu);
            }

            private final MenuItem.OnMenuItemClickListener onEditMenu = new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    final int position=getAdapterPosition();

                    switch (item.getItemId()) {
                        case 1:
                            final Dialog dialog=new Dialog(RecyclerViewActivity.this);
                            dialog.setContentView(R.layout.update_dialog_layout);
                            dialog.setTitle("Edit");
                            final EditText edt=dialog.findViewById(R.id.edt_object_content);
                            edt.setText(data.get(position));
                            Button btnCancel=dialog.findViewById(R.id.btn_cancel);
                            Button btnSave=dialog.findViewById(R.id.btn_save);

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                            btnSave.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String newValue=edt.getText().toString();
                                    data.set(position,newValue);
                                    adapter.notifyDataSetChanged();
                                    dialog.dismiss();
                                }
                            });

                            dialog.show();
                            break;

                        case 2:
                            data.remove(position);
                            adapter.notifyItemRemoved(position);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            };
        }

    }

    public class DividerItemDecoration extends RecyclerView.ItemDecoration{
        private Drawable mDivider;
        public DividerItemDecoration(Drawable divider) {
            mDivider = divider;
        }
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent.getChildAdapterPosition(view) == 0) {
                return;
            }

            outRect.top = mDivider.getIntrinsicHeight();
        }

        @Override
        public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int dividerTop = child.getBottom() + params.bottomMargin;
                int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

                mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                mDivider.draw(canvas);
            }
        }
    }

}
