package info.androidhive.recyclerviewsearch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
        Date date=simpleDateFormat.parse("22/8/1995");
        System.out.println(date.getDay()+"/"+date.getMonth()+"/"+date.getYear());
    }

}
