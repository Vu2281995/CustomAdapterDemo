package info.androidhive.recyclerviewsearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by levan on 12/14/2017.
 */

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.StaffHolder> implements Filterable{

    private Context context;

    private ArrayList<Staff> listStaff;

    private ArrayList<Staff> listStaffFiltered;

    private int resource;

    private StaffsAdapterListener staffsAdapterListener;

    public StaffAdapter(Context context, ArrayList<Staff> listStaff, int resource) {
        this.context = context;
        this.listStaff = listStaff;
        this.listStaffFiltered=listStaff;
        this.resource = resource;
    }

    @Override
    public StaffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(resource,parent,false);
        StaffHolder holder=new StaffHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(StaffHolder holder, int position) {
        Staff staff=listStaffFiltered.get(position);
        holder.txtName.setText(staff.getName());
        holder.txtUnit.setText(staff.getUnit());
        holder.txtMail.setText(staff.getEmail());
    }

    @Override
    public int getItemCount() {
        return listStaffFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listStaffFiltered = listStaff;
                } else {
                    ArrayList<Staff> filteredList = new ArrayList<>();
                    for (Staff staff : listStaff) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (staff.getName().toLowerCase().contains(charString.toLowerCase()) || staff.getUnit().contains(charSequence)) {
                            filteredList.add(staff);
                        }
                    }

                    listStaffFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listStaffFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listStaffFiltered = (ArrayList<Staff>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class StaffHolder extends RecyclerView.ViewHolder{

        private TextView txtName,txtUnit,txtMail;

        public StaffHolder(View itemView) {
            super(itemView);

            txtName=itemView.findViewById(R.id.txt_name);
            txtUnit=itemView.findViewById(R.id.txt_unit);
            txtMail=itemView.findViewById(R.id.txt_email);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (staffsAdapterListener!=null){
                        staffsAdapterListener.onStaffSelected(listStaffFiltered.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public interface StaffsAdapterListener {
        void onStaffSelected(Staff staff);
    }

    public void setStaffsAdapterListener(StaffsAdapterListener staffsAdapterListener){
        this.staffsAdapterListener=staffsAdapterListener;
    }
}
