package info.androidhive.recyclerviewsearch;

/**
 * Created by levan on 12/14/2017.
 */

public class Staff {

    private String name;

    private String unit;

    private String email;

    public Staff(String name, String unit, String email) {
        this.name = name;
        this.unit = unit;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
