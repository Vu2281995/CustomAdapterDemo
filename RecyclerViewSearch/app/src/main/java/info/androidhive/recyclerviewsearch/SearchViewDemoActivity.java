package info.androidhive.recyclerviewsearch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;

public class SearchViewDemoActivity extends AppCompatActivity {

    private RecyclerView recyclerStaff;

    private EditText edtSearch;

    private StaffAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view_demo);

        recyclerStaff=findViewById(R.id.recyclerStaff);

        edtSearch=findViewById(R.id.edt_search);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerStaff.setLayoutManager(linearLayoutManager);

        adapter=new StaffAdapter(SearchViewDemoActivity.this,getData(),R.layout.staff_row_item);

        recyclerStaff.setAdapter(adapter);
    }

    private ArrayList<Staff> getData(){
        ArrayList<Staff> listStaffs=new ArrayList<>();

        listStaffs.add(new Staff("Lê Văn Vũ","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Nguyễn Văn Phong","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Đoàn Hữu Tư","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Hoàng Văn Thái","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Trịnh Xuân Thanh","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Nguyễn Phú Trọng","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Nguyễn Tấn Dũng","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Trần Đại Quang","Công ty MISA","levanvu@gmail.com"));
        listStaffs.add(new Staff("Đào Quang Hà","Công ty MISA","levanvu@gmail.com"));

        return listStaffs;
    }
}
